package facci.pm.carlos.ramirezruiz.multitasking;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button button;
    Button button2;
    Button button3;
    Button button4;
    ProgressBar progressBar;
    private final String TAG_LOG = "test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button =  (Button)findViewById(R.id.button);
        button2 =  (Button)findViewById(R.id.button2);
        button3 =  (Button)findViewById(R.id.button3);
        progressBar =  (ProgressBar) findViewById(R.id.progressBar);
        button4 = (Button)findViewById(R.id.button4);



        button.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast toast = Toast.makeText(getApplicationContext(), "...Haciendo otra cosa el usuario sobre el hilo " +
                        "PRINCIPAL a la vez que carga...", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 1075);
                toast.show();
            }
        });


    }

    private void UnSegundo(){
        try{
            Thread.sleep(1000);
        }catch (InterruptedException e){}
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.button:
                for(int i=1; i<=10; i++){
                    UnSegundo();
                }


                break;
            case R.id.button2:


                Hilos();

                break;
            case R.id.button3:

                EjemploAsyncTask ejemploAsyncTask = new EjemploAsyncTask();
                ejemploAsyncTask.execute();



                break;
            default:
                break;
        }

    }

    void Hilos(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=1; i<=8; i++){
                    UnSegundo();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getBaseContext(),"Tarea Larga Finalizada",Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }).start();
    }


    private class EjemploAsyncTask extends AsyncTask<Void,Integer,Boolean>{
        public EjemploAsyncTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setMax(100);
            progressBar.setProgress(0);

        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            for (int i = 1; i <= 10; i++) {
                UnSegundo();
                publishProgress(i * 10);
                if (isCancelled()) {
                    break;
                }

            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            progressBar.setProgress(values[0].intValue());
        }

        @Override
        protected void onPostExecute(Boolean resultado) {
            //super.onPostExecute(boolean);

            if(resultado){
                Toast.makeText(getBaseContext(),"Tarea Larga Finalizada en AsyncTask",Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getBaseContext(),"Tarea Larga Ha sido Cancelada",Toast.LENGTH_SHORT).show();
        }


    }


}
